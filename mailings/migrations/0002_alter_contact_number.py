# Generated by Django 4.1 on 2022-08-21 17:38

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailings', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='number',
            field=models.CharField(max_length=11, unique=True, validators=[django.core.validators.RegexValidator(message="Номер телефона необходимо вводить в формате: '7XXXXXXXXX'. разрешено только 11 цифр. (X - цифра от 0 до 9)", regex='^7\\d{10}$')]),
        ),
    ]
